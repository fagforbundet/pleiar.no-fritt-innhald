autoTest:
	@if [ "`basename "$(shell realpath $$PWD)"`" == "data" ]; then make test; else make ciTest;fi
distTest: test
test: _checkDir
	@make -C .. testData
_checkDir:
	@if [ ! -e ../Makefile ] || [ ! -e ../tests/main.Pleiar.test.js ]; then echo "Datamappa ser ikkje ut til å vere i eit pleiar.no mappetre";exit 1;fi
ciTest:
	rm -rf pleiar-test-dist
	if [ ! -e node_modules ] && [ -e ../node_modules ]; then ln -sf ../node_modules .;fi
	if [ ! -e .cache ] && [ -e ../.cache ]; then ln -sf ../.cache .;fi
	mkdir -p node_modules .cache
	git clone git@gitlab.com:fagforbundet/pleiar.no pleiar-test-dist
	cd pleiar-test-dist; git clone ../ data
	cd pleiar-test-dist; ln -s $(shell pwd)/.cache .
	cd pleiar-test-dist; ln -s $(shell pwd)/node_modules . && yarn install --no-progress --silent
	make -C pleiar-test-dist testData
	rm -rf pleiar-test-dist
