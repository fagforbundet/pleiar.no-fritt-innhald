# Pleiar.no public dataset

Pleiar.no is a set of tools for nurses to use in their day-to-day work, or
nursing students to use as learning aids. They are all in Norwegian (though the
source code and repository is commented in english). You can find the source
code for pleiar.no at https://gitlab.com/fagforbundet/pleiar.no

This repository contains a stubbed dataset that you can use with the pleiar.no
source code to get a functional instance of pleiar.no, but it does not contain
any of the (non-free) content available on the actual production website.
