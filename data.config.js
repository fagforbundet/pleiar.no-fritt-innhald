/* eslint-disable flowtype/require-valid-file-annotation */
/* globals module */
module.exports = {
    // Sets the meta "theme-color" property in the HTML
    themeColor: '#343a40',
    // Used as the parameter for OfflinePlugin's externals setting
    cacheExternals: [],
    // The copyright header used for data files
    copyrightHeader: 'Pleiar.no\nCopyright (C) Eskild Hustvedt, Fagforbundet\nLicensed under the GNU Affero General Public License version 3',
    // Configures our authentication system. It is disabled in this instance.
    authentication:
    {
        production:
        {
            defaultState: true,
            url: 'https://example.com/sso',
            app_id:'no.pleiar',
        },
        development: {
            defaultState: true,
            url: 'https://example.com/test-sso',
            app_id: 'no.pleiar',
        }
    }
};
