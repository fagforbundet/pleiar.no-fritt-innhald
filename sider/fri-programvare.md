### Pleiar.no er fri programvare

Det inneber at du kan køyre, kopiere, distribuere, studere, endre og forbetre *programvaren* fritt, under visse vilkår. Sjå [fri-programvare definisjonen](https://www.gnu.org/philosophy/free-sw.html) for meir informasjon om fri programvare.

#### Lisens for kjeldekode

Du kan redistribuere og/eller endre dette programmet under vilkåra i [GNU Affero General Public License](https://www.gnu.org/licenses/agpl.html), som publisert av [Free Software Foundation](https://www.fsf.org/), anten [versjon 3](https://www.gnu.org/licenses/agpl-3.0.html) av lisensen, eller (om du vil) ein seinare versjon.

Programmet vart distribuert i håp om at det er nyttig, men UTAN NOKON FORM FOR GARANTI; ikkje ein gang ein implisitt garanti om SALGBARHET eller EGNETHET TIL EIT SPESIELT FORMÅL. Sjå [GNU Affero General Public License](https://www.gnu.org/licenses/agpl.html) for meir informasjon.

Kjeldekoden til Pleiar.no er tilgjengeleg på [gitlab](https://gitlab.com/zerodogg/pleiar.no). Viss denne instansen av Pleiar.no har verta endra frå koden som er tilgjengeleg på gitlab har du rett på ein full kopi av den endra kjeldekoden, jfr. GNU Affero General Public License.

#### Lisens for innhald

Innhaldet på pleiar.no er ikkje fri programvare. Det er opphavsrettsleg
beskytta etter andsverklova. Kopiering, redistribuering, endring og gjenbruk er
ikkje tillete utan eksplisitt skriftleg tillatelse frå Fagforbundet. Eit
avgrensa datasett er tilgjengeleg for fri bruk på
[gitlab.com](https://gitlab.com/fagforbundet/pleiar.no-fritt-innhald).
