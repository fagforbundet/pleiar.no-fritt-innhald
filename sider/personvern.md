### Personvern

#### Data
Pasientinformasjon er sensitive opplysningar. Pleiar.no utførar all utrekning og handlingar lokalt i nettlesaren. Etter at du har lasta inn Pleiar.no vil ingen fleire førespurnadar bli sendt til Pleiar.no sin tenar, forutan å sjekke etter ein oppdatert versjon av Pleiar.no. Data du skriv inn i Pleiar.no vart difor _aldri_ sendt til tenaren til Pleiar.no. Alt skjer på din lokale datamaskin, mobiltelefon o.l. - data forlet aldri den lokale maskinen.

#### Øvrig

Pleiar.no er sikra med TLS. Sida nyttar ikkje cookies. Det einaste som vart lagra er ein logg over besøkte sider for statistiske formål - denne loggen vart jamnleg sletta. Sida nyttar heller ingen eksterne skript, stilar o.l., besøk sender difor ingen førespurnadar utanfor Pleiar.no.
