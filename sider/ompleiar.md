### Pleiar.no

Pleiar.no is a set of tools for nurses to use in their day-to-day work, or
nursing students to use as learning aids. They are all in Norwegian (though the
source code and repository is commented in english).

Pleiar.no is written in javascript, uses React.js, Redux.js and a variant of
Bootstrap 4 for React applications called reactstrap. The CSS is written in
SASS, and the whole thing is compiled using webpack. Utilities of note that are
used include babel, flow and react-snap.
